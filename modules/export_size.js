const fs = require("fs");
var sizeOf = require("image-size");

let dir = process.cwd();
let result = "";
//для локального запуска
// let files = fs
//   .readdirSync("./../../PSD")
//   .filter(el => el.indexOf(".psd") !== -1);
// files.forEach(el => {
//   var stats = sizeOf(`./../../PSD/${el}`);
//   if (stats.type !== "psd") return;
//   result =
//     result + `width: ${stats.width}, height: ${stats.height}, name: ${el}\n`;
// });

//для глобального запуска
console.log(dir);
let files = fs.readdirSync(dir).filter(el => el.indexOf(".psd") !== -1);

if (files.length > 0) {
  files.forEach(el => {
    var stats = sizeOf(`${dir}/${el}`);
    if (stats.type !== "psd") return;
    result =
      result + `width: ${stats.width}, height: ${stats.height}, name: ${el}\n`;
  });
  fs.writeFile("sizesPSD.txt", result, function(err) {
    if (err) throw err;
    console.log(result);
    console.log("The file was created!");
  });
} else {
  console.log("no psd files!");
}

//для того что бы терминал закрывался не сразу
setTimeout(() => {
  console.log("tut");
}, 40000);
