const fs = require("fs");
var child_process = require("child_process");
let dir = fs.readdirSync("C:/");

function copy() {
  fs.mkdirSync("C://export_size_for_PSD");
  fs.createReadStream("./export_size_for_PSD/export_size.exe").pipe(
    fs.createWriteStream("C://export_size_for_PSD/export_size.exe")
  );
}

function deleteFolderRecursive(path) {
  if (fs.existsSync(path)) {
    fs.readdirSync(path).forEach(function(file, index) {
      var curPath = path + "/" + file;
      if (fs.lstatSync(curPath).isDirectory()) {
        // recurse
        deleteFolderRecursive(curPath);
      } else {
        // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
}

if (dir.indexOf("export_size_for_PSD") === -1) {
  copy();
  child_process.execSync("export_size.reg");
} else {
  deleteFolderRecursive("C://export_size_for_PSD");
  setTimeout(() => {
    copy();
    child_process.execSync("export_size.reg");
  }, 1000);
}
