const fs = require('fs')
var sizeOf = require('image-size')
const readline = require('readline')

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

let dir = ''

rl.question('Please enter the path to the folder with psd\n', answer => {
  dir = answer
  console.log('dir', dir)
  let result = ''

  let files = fs.readdirSync(dir).filter(el => el.indexOf('.psd') !== -1)

  if (files.length > 0) {
    files.forEach(el => {
      var stats = sizeOf(`${dir}/${el}`)
      if (stats.type !== 'psd') return
      result =
        result + `width: ${stats.width}, height: ${stats.height}, name: ${el}\n`
    })
    fs.writeFile(`${dir}/sizesPSD.txt`, result, function(err) {
      if (err) throw err
      console.log(result)
      console.log('The file was created!')
    })
  } else {
    console.log('no psd files!')
  }

  setTimeout(() => {
    console.log('tut')
  }, 40000)
  rl.close()
})
