var fs = require('fs');
let dir = fs.readdirSync("./");
function copy() {
    fs.mkdirSync("./build");
    fs.mkdirSync("./build/export_size_for_PSD");
    fs.createReadStream("export_size.exe").pipe(
      fs.createWriteStream("./build/export_size_for_PSD/export_size.exe")
    );
    fs.createReadStream("./README.MD").pipe(
        fs.createWriteStream("./build/README.txt")
    );
    fs.createReadStream("./export_size.reg").pipe(
        fs.createWriteStream("./build/export_size.reg")
    );
    fs.createReadStream("./modules/replace_export_size.js").pipe(
      fs.createWriteStream("./build/replace_export_size.js")
    );
}

function deleteFolderRecursive(path) {
    if (fs.existsSync(path)) {
      fs.readdirSync(path).forEach(function(file, index) {
        var curPath = path + "/" + file;
        if (fs.lstatSync(curPath).isDirectory()) {
          // recurse
          deleteFolderRecursive(curPath);
        } else {
          // delete file
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(path);
    }
  }
  
  if (dir.indexOf("build") === -1) {
    copy();
  } else {
    deleteFolderRecursive("./build");
    setTimeout(() => {
      copy();
    }, 1000);
  }